class TodoRepository {
  async setup() {}
  async destroy() {}

  async cleanTodos() {}
  async getAll() {}
  async getById(id) {}
  async save(todo) {}
  async createTodo(name) {}
  async deleteTodo(id) {}
  async updateTodo(id, name) {}
}

module.exports = TodoRepository;
