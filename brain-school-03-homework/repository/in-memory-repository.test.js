const InMemoryRepository = require("./in-memory-repository.js");

describe("in memory repository", () => {
  let todoRepository;
  beforeEach(() => {
    todoRepository = new InMemoryRepository();
  });

  describe("read todos", () => {
    it("return an empty list of todos", async () => {
      const todosData = await todoRepository.getAll();
      expect(todosData).toEqual([]);
    });
    it("return a list of todos", async () => {
      await todoRepository.createTodo("Task1");
      await todoRepository.createTodo("Task2");
      await todoRepository.createTodo("Task3");
      const todosData = await todoRepository.getAll();
      expect(todosData).toEqual([
        { name: "Task1", id: expect.any(String) },
        { name: "Task2", id: expect.any(String) },
        { name: "Task3", id: expect.any(String) },
      ]);
    });
    it("return a specific todo from a list of many", async () => {
      await todoRepository.createTodo("Task1");
      await todoRepository.createTodo("Task2");
      await todoRepository.createTodo("Task3");

      const todosAllData = await todoRepository.getAll();
      const todosData = await todoRepository.getById(todosAllData[1].id);
      expect(todosData).toEqual({ name: "Task2", id: expect.any(String) });
    });
  });
  describe("create todos", () => {
    it("create a todo", async () => {
      await todoRepository.createTodo("Task1");
      const todosData = await todoRepository.getAll();
      expect(todosData).toEqual([{ name: "Task1", id: expect.any(String) }]);
    });
  });
  describe("delete todos", () => {
    it("delete a todo from a list of many", async () => {
      await todoRepository.createTodo("Task1");
      await todoRepository.createTodo("Task2");
      await todoRepository.createTodo("Task3");

      const todosAllData = await todoRepository.getAll();
      const specificTodoData = await todoRepository.getById(todosAllData[1].id);

      await todoRepository.deleteTodo(specificTodoData.id);
      const todosDelData = await todoRepository.getAll();
      expect(todosDelData).toEqual([
        { name: "Task1", id: expect.any(String) },
        { name: "Task3", id: expect.any(String) },
      ]);
    });
  });
  describe("update todos", () => {
    it("update a todo from a list of many", async () => {
      await todoRepository.createTodo("Task1");
      await todoRepository.createTodo("Task2");
      await todoRepository.createTodo("Task3");

      const todosAllData = await todoRepository.getAll();
      const specificTodoData = await todoRepository.getById(todosAllData[1].id);
      await todoRepository.updateTodo(specificTodoData.id, "newName");
      const todosUpdateData = await todoRepository.getAll();
      expect(todosUpdateData).toEqual([
        { name: "Task1", id: expect.any(String) },
        { name: "newName", id: expect.any(String) },
        { name: "Task3", id: expect.any(String) },
      ]);
    });
  });
});
