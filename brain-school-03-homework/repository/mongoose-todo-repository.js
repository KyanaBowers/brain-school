const TodoRepository = require("./todos-repository");
const Todos = require("./todos.js");
const mongoose = require("mongoose");

class MongooseTodoRepository extends TodoRepository {
  async setup() {
    await mongoose.connect("mongodb://localhost/bit-by-bit", {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    this.TodoSchema = new mongoose.Schema({
      _id: String,
      name: String,
    });
    this.TodoModel = mongoose.model("Todo", this.TodoSchema);
  }

  async destroy() {
    await mongoose.connection.close();
  }

  async cleanTodos() {
    await this.TodoModel.deleteMany({});
    mongoose.deleteModel(/.+/);
  }

  convertFromMongo(todo) {
    if (todo) {
      return {
        id: todo._id,
        name: todo.name,
      };
    }
    return null;
  }

  convertToMongo(todo) {
    if (todo) {
      return {
        _id: todo.id,
        name: todo.name,
      };
    }
    return null;
  }

  async getAll() {
    const todos = await this.TodoModel.find();
    return todos.map(this.convertFromMongo);
  }

  async getById(id) {
    const todo = await this.TodoModel.findById(id);
    return this.convertFromMongo(todo);
  }

  async save(todo) {
    const mongoTodo = new this.TodoModel(this.convertToMongo(todo));
    await mongoTodo.save();
  }
  async createTodo(name) {
    const todo = new Todos(name);
    await this.save(todo);
  }

  async deleteTodo(id) {
    await this.TodoModel.findByIdAndDelete(id);
  }

  async updateTodo(id, name) {
    await this.TodoModel.findByIdAndUpdate(id, { name: name });
  }
}

module.exports = MongooseTodoRepository;
