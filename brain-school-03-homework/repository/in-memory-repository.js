const TodoRepository = require("./todos-repository.js");
const Todo = require("./todos.js");

class InMemoryRepository extends TodoRepository {
  constructor() {
    super();
    this.todos = new Map();
  }
  async cleanTodos() {
    this.todos.clear();
  }

  async getAll() {
    const iterator = this.todos.values();
    return Array.from(iterator);
  }

  async getById(id) {
    return this.todos.get(id);
  }

  async createTodo(name) {
    const todo = new Todo(name);
    this.save(todo);
  }

  async save(todo) {
    this.todos.set(todo.id, todo);
  }

  async deleteTodo(id) {
    this.todos.delete(id);
  }

  async updateTodo(id, newName) {
    const todo = this.todos.get(id);
    todo.name = newName;
    this.todos.set(todo.id, todo);
  }
}
module.exports = InMemoryRepository;
