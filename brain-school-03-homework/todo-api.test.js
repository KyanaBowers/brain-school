const TodoApi = require("./todo-api.js");
const InMemoryRepository = require("./repository/in-memory-repository.js");
const MongooseTodoRepository = require("./repository/mongoose-todo-repository.js");

describe("todo api", () => {
  let api;
  let memoryRepository;
  beforeEach(async () => {
    //memoryRepository = new InMemoryRepository();
    memoryRepository = new MongooseTodoRepository();
    await memoryRepository.setup();
    api = new TodoApi(memoryRepository);
    await api.cleanTodos();
  });

  afterEach(async () => {
    await memoryRepository.destroy();
  });

  describe("list todos", () => {
    it("get an empty list of todos", async () => {
      const data = await api.getTodos();
      expect(data).toEqual([]);
    });
    it("get a list of todos", async () => {
      await api.createTodo("Task1");
      await api.createTodo("Task2");
      await api.createTodo("Task3");
      const data = await api.getTodos();
      expect(data).toEqual([
        { name: "Task1", id: expect.any(String) },
        { name: "Task2", id: expect.any(String) },
        { name: "Task3", id: expect.any(String) },
      ]);
    });
    it("get a single todo from a list of many", async () => {
      await api.createTodo("Task1");
      await api.createTodo("Task2");
      await api.createTodo("Task3");
      const data = await api.getTodos();
      const specificTodo = await api.getSpecificTodo(data[1].id);
      expect(specificTodo).toEqual({ name: "Task2", id: expect.any(String) });
    });
  });

  describe("add todos", () => {
    it("add one todo", async () => {
      await api.createTodo("Task1");
      const data = await api.getTodos();
      expect(data).toEqual([{ name: "Task1", id: expect.any(String) }]);
    });
  });

  describe("delete todos", () => {
    it("deletes one todo from a list of many", async () => {
      await api.createTodo("Task1");
      await api.createTodo("Task2");
      await api.createTodo("Task3");

      const dataBeforeDeletion = await api.getTodos();
      await api.deleteTodo(dataBeforeDeletion[1].id);
      const dataAfterDeletion = await api.getTodos();
      expect(dataAfterDeletion).toEqual([
        { name: "Task1", id: expect.any(String) },
        { name: "Task3", id: expect.any(String) },
      ]);
    });
  });

  describe("update todos", () => {
    it("update one todo from a list of many", async () => {
      await api.createTodo("Task1");
      await api.createTodo("Task2");
      await api.createTodo("Task3");

      const dataBeforeUpdate = await api.getTodos();
      await api.updateTodo(dataBeforeUpdate[1].id, "NewName");
      const dataAfterUpdate = await api.getTodos();
      expect(dataAfterUpdate).toEqual([
        { name: "Task1", id: expect.any(String) },
        { name: "NewName", id: expect.any(String) },
        { name: "Task3", id: expect.any(String) },
      ]);
    });
  });
});
