class TodoApi {
  constructor(todoRepository) {
    this.todoRepository = todoRepository;
  }

  async cleanTodos() {
    await this.todoRepository.cleanTodos();
  }

  async getTodos() {
    return await this.todoRepository.getAll();
  }

  async getSpecificTodo(id) {
    return await this.todoRepository.getById(id);
  }

  async createTodo(name) {
    await this.todoRepository.createTodo(name);
  }

  async deleteTodo(id) {
    await this.todoRepository.deleteTodo(id);
  }

  async updateTodo(id, name) {
    await this.todoRepository.updateTodo(id, name);
  }
}

module.exports = TodoApi;
