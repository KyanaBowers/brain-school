const request = require("supertest");
const app = require("./server.js");
const InMemoryRepository = require("./repository/in-memory-repository");
const Todo = require("./repository/todos");

describe("todo-api", () => {
  let server;
  let todoRepository;
  beforeEach(async () => {
    todoRepository = new InMemoryRepository();
    server = new app(todoRepository);
    await server.start();
  });

  afterEach(async () => {
    await server.stop();
  });

  describe("get todos", () => {
    it("returns a empty list of todos", async () => {
      const response = await request(server.app).get("/todos");
      expect(response.status).toEqual(200);
      expect(response.body).toMatchObject({ todos: [] });
    });
    it("returns a list of todos", async () => {
      await todoRepository.createTodo("Task1");
      await todoRepository.createTodo("Task2");
      const response = await request(server.app).get("/todos");
      expect(response.status).toEqual(200);
      expect(response.body).toMatchObject({
        todos: [
          { id: expect.any(String), name: "Task1" },
          { id: expect.any(String), name: "Task2" },
        ],
      });
    });

    it("returns a HAL resource with a self link", async () => {
      const response = await request(server.app).get("/todos");
      expect(response.body._links.self).toEqual({
        href: "/todos",
      });
    });

    it("returns a HAL resource with a create link", async () => {
      const response = await request(server.app).get("/todos");
      expect(response.body._links.create).toEqual({
        href: "/todos",
        method: "POST",
      });
    });

    it("returns todos as HAL resources with links", async () => {
      const todo = new Todo("Task1");
      await todoRepository.save(todo);

      const response = await request(server.app).get("/todos");
      const todoLinks = response.body.todos[0]._links;
      expect(todoLinks.collection).toEqual({
        href: `/todos`,
      });
      expect(todoLinks.self).toEqual({
        href: `/todos/${todo.id}`,
      });
      expect(todoLinks.delete).toEqual({
        href: `/todos/${todo.id}`,
        method: "DELETE",
      });
      expect(todoLinks.update).toEqual({
        href: `/todos/${todo.id}`,
        method: "PUT",
      });
    });
  });

  describe("get a single todo", () => {
    let testTodo;
    beforeEach(async () => {
      await todoRepository.createTodo("Task1");
      testTodo = new Todo("Task2");
      await todoRepository.save(testTodo);
      await todoRepository.createTodo("Task3");
    });
    it("returns a single todo in a list of todos", async () => {
      const singleTodoId = testTodo.id;
      const response = await request(server.app).get(`/todos/${singleTodoId}`);
      expect(response.status).toEqual(200);
      expect(response.body).toMatchObject({
        id: expect.any(String),
        name: "Task2",
      });
    });

    it("returns a todo as a HAL resource with a self link", async () => {
      const response = await request(server.app).get(`/todos/${testTodo.id}`);
      expect(response.status).toEqual(200);
      expect(response.body._links.self).toEqual({
        href: `/todos/${testTodo.id}`,
      });
    });
    it("returns a HAL resource with a collection link", async () => {
      const response = await request(server.app).get(`/todos/${testTodo.id}`);
      expect(response.body._links.collection.href).toBe("/todos");
    });
    it("returns a HAL resource with a delete link", async () => {
      const response = await request(server.app).get(`/todos/${testTodo.id}`);
      expect(response.body._links.delete.href).toBe(`/todos/${testTodo.id}`);
      expect(response.body._links.delete.method).toBe("DELETE");
    });
    it("returns a HAL resource with an update link", async () => {
      const response = await request(server.app).get(`/todos/${testTodo.id}`);
      expect(response.body._links.update.href).toBe(`/todos/${testTodo.id}`);
      expect(response.body._links.update.method).toBe("PUT");
    });
  });

  describe("create todos", () => {
    it("creates one todo", async () => {
      const response = await request(server.app)
        .post("/todos")
        .send({ name: "Task1" });
      expect(response.status).toEqual(201);
      expect(await todoRepository.getAll()).toEqual([
        { id: expect.any(String), name: "Task1" },
      ]);
    });

    it("creates many todos", async () => {
      await request(server.app).post("/todos").send({ name: "Task1" });
      await request(server.app).post("/todos").send({ name: "Task2" });
      await request(server.app).post("/todos").send({ name: "Task3" });
      expect(await todoRepository.getAll()).toEqual([
        { id: expect.any(String), name: "Task1" },
        { id: expect.any(String), name: "Task2" },
        { id: expect.any(String), name: "Task3" },
      ]);
    });
  });

  describe("delete todos", () => {
    it("delete one todo out of a list of one", async () => {
      const todo = new Todo("Task1");
      await todoRepository.save(todo);

      const delResponse = await request(server.app).delete(`/todos/${todo.id}`);
      expect(delResponse.status).toBe(204);

      expect(await todoRepository.getAll()).toEqual([]);
    });

    it("delete 2nd todo out of a list of many", async () => {
      await todoRepository.createTodo("Task1");
      const todo = new Todo("Task2");
      await todoRepository.save(todo);
      await todoRepository.createTodo("Task3");

      const delResponse = await request(server.app).delete(`/todos/${todo.id}`);
      expect(delResponse.status).toBe(204);

      expect(await todoRepository.getAll()).toEqual([
        { name: "Task1", id: expect.any(String) },
        { name: "Task3", id: expect.any(String) },
      ]);
    });
  });

  describe("update todos", () => {
    it("updates one todo in a list of one", async () => {
      const todo = new Todo("Task1");
      await todoRepository.save(todo);

      const response = await request(server.app)
        .put(`/todos/${todo.id}`)
        .send({ name: "newName" });
      expect(response.status).toBe(200);

      expect(await todoRepository.getAll()).toEqual([
        { id: expect.any(String), name: "newName" },
      ]);
    });

    it("updates 2nd todo in a list of many", async () => {
      await todoRepository.createTodo("Task1");
      const todo = new Todo("Task2");
      await todoRepository.save(todo);
      await todoRepository.createTodo("Task3");

      const response = await request(server.app)
        .put(`/todos/${todo.id}`)
        .send({ name: "newName" });
      expect(response.status).toBe(200);

      expect(await todoRepository.getAll()).toEqual([
        { id: expect.any(String), name: "Task1" },
        { id: expect.any(String), name: "newName" },
        { id: expect.any(String), name: "Task3" },
      ]);
    });
  });
});
