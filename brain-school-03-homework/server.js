const express = require("express");
const { v4: uuidv4 } = require("uuid");
const Api = require("./todo-api.js");
const halson = require("halson");

class serverApp {
  constructor(todoRepository) {
    this.todoApi = new Api(todoRepository);

    this.app = express();
    this.app.use(express.json());

    this.app.get("/todos", async (req, res) => {
      const todos = await this.todoApi.getTodos();
      const hal = halson({
        todos: todos.map(this.addHalLinksToTodo),
      });
      hal.addLink("self", "/todos");
      hal.addLink("create", {
        href: "/todos",
        method: "POST",
      });
      res.status(200).send(hal);
    });

    this.app.get("/todos/:id", async (req, res) => {
      const todo = await this.todoApi.getSpecificTodo(req.params.id);
      const hal = this.addHalLinksToTodo(todo);
      res.status(200).send(hal);
    });

    this.app.post("/todos", async (req, res) => {
      await this.todoApi.createTodo(req.body.name);
      res.status(201).send();
    });
    this.app.delete("/todos/:id", async (req, res) => {
      await this.todoApi.deleteTodo(req.params.id);
      res.status(204).send();
    });
    this.app.put("/todos/:id", async (req, res) => {
      await this.todoApi.updateTodo(req.params.id, req.body.name);
      res.status(200).send();
    });
  }

  addHalLinksToTodo(todo) {
    const hal = halson(todo);
    hal.addLink("self", `/todos/${todo.id}`);
    hal.addLink("collection", "/todos");
    hal.addLink("delete", {
      href: `/todos/${todo.id}`,
      method: "DELETE",
    });
    hal.addLink("update", {
      href: `/todos/${todo.id}`,
      method: "PUT",
    });
    return hal;
  }

  start() {
    const serverPromise = new Promise((resolve) => {
      this.server = this.app.listen(3000, () => {
        resolve();
      });
    });
    return serverPromise;
  }

  stop() {
    const serverPromise = new Promise((resolve) => {
      this.server.close(() => {
        this.todoApi.cleanTodos();
        resolve();
      });
    });
    return serverPromise;
  }
}

module.exports = serverApp;
