const http = require("http");

const server = http.createServer();

const requestListener = (request, response) => {
  const requestMethod = request.requestMethod;

  if (requestMethod === "GET") {
    const jsonData = JSON.stringify({
      studentName: "Gerson Jepzir",
      hobbies: ["yelling", "petting cats"],
    });
    response.statusCode = 200;
    response.setHeader("Content-Type", "application/json");
    response.write(jsonData);
    response.end();
  } else {
    response.statusCode = 405;
    response.end();
  }
};

server.on("request", requestListener);

server.listen(8080);
