# JS Bundling Part 2 of 2 - Build your own Webpack Style Bundler

## How to use this repository

### Install dependencies

`npm i`

### Run the tests to generate a bundle

If you are running the tests for the first time on your machine: ``npm run test -u`. This will update the snapshots with the absolute path to the source files on your machine.  

After updating the snapshots, run `npm run test-watch` to have the tests run continuosly while you are working.

The bundled code can be found at `src/.tmp`

### Execute the bundled code

In your terminal run the following: `node src/.tmp`
