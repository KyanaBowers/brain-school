const createRuntime = require("./create-runtime.js");
describe("create-runtime", () => {
  describe("given an entry point,", () => {
    describe("and a module map containing a single module", () => {
      it("returns a runtime function", () => {
        const moduleMap = `{ '/home/kyana/code/brain-school/02-js-bundling-part-2-of-2/src/tdd/test-modules/folder-a/folder-b/module-b.js': function (exports,require){ const b = "bebe";
        exports.default = b; } }`;
        const entryPoint =
          "/home/kyana/code/brain-school/02-js-bundling-part-2-of-2/src/tdd/test-modules/folder-a/folder-b/module-b.js";
        expect(createRuntime(moduleMap, entryPoint)).toMatchInlineSnapshot(`
          "const modules = { '/home/kyana/code/brain-school/02-js-bundling-part-2-of-2/src/tdd/test-modules/folder-a/folder-b/module-b.js': function (exports,require){ const b = "bebe";
          exports.default = b; } };
          const entry = "/home/kyana/code/brain-school/02-js-bundling-part-2-of-2/src/tdd/test-modules/folder-a/folder-b/module-b.js";
          function webpackStart({modules,entry}){
          const moduleCache = {};
          //Webpack require function
          const require = moduleName => {
          //If the module is cached, return the cached version
          if(moduleCache[moduleName]){
          return moduleCache[moduleName];
          }
          const exports = {};
          //We need to avoid the cyclical dependencies
          //when invoking require()
          moduleCache[moduleName] = exports;
          //require() the module 
          modules[moduleName](exports,require);
          return moduleCache[moduleName];
          };
          //Execute the program
          require(entry);
          }
          webpackStart({modules,entry});
          "
        `);
      });
    });
    describe("and a module map containing a single module", () => {
      it("returns a runtime function", () => {
        const moduleMap = `{ 'home/kyana/code/brain-school/02-js-bundling-part-2-of-2/src/tdd/test-modules/folder-a/folder-b/module-b.js': function (exports,require){ const b = "bebe";
          exports.default = b; } }`;
        const entryPoint =
          "/code/brain-school/02-js-bundling-part-2-of-2/src/tdd/test-modules/folder-a/folder-b/module-b.js";
        expect(createRuntime(moduleMap, entryPoint)).toMatchInlineSnapshot(`
          "const modules = { 'home/kyana/code/brain-school/02-js-bundling-part-2-of-2/src/tdd/test-modules/folder-a/folder-b/module-b.js': function (exports,require){ const b = "bebe";
          exports.default = b; } };
          const entry = "/code/brain-school/02-js-bundling-part-2-of-2/src/tdd/test-modules/folder-a/folder-b/module-b.js";
          function webpackStart({modules,entry}){
          const moduleCache = {};
          //Webpack require function
          const require = moduleName => {
          //If the module is cached, return the cached version
          if(moduleCache[moduleName]){
          return moduleCache[moduleName];
          }
          const exports = {};
          //We need to avoid the cyclical dependencies
          //when invoking require()
          moduleCache[moduleName] = exports;
          //require() the module 
          modules[moduleName](exports,require);
          return moduleCache[moduleName];
          };
          //Execute the program
          require(entry);
          }
          webpackStart({modules,entry});
          "
        `);
      });
    });
  });
});
