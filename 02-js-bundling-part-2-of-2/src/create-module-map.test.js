const path = require("path");
const createModuleDependencyGraph = require("./create-module-dependency-graph.js");
const createModuleMap = require("./create-module-map.js");
describe("create-module-map", () => {
  describe("given a module dependency graph with a single module", () => {
    it("returns a module map with single entry", () => {
      const moduleDependencyGraph = createModuleDependencyGraph(
        __dirname,
        "./test-modules/folder-a/folder-b/module-b.js"
      );

      expect(createModuleMap(moduleDependencyGraph)).toMatchInlineSnapshot(`
        "{ '/home/kyana/code/brain-school/02-js-bundling-part-2-of-2/src/test-modules/folder-a/folder-b/module-b.js': function (exports,require){ const b = "bebe";
        exports.default = b; } }"
      `);
    });
  });
  describe("given a module dependency graph with multiple, dependent modules", () => {
    it("returns a module map with an entry for each module", () => {
      const moduleDependencyGraph = createModuleDependencyGraph(
        __dirname,
        "./test-modules/module-c.js"
      );

      expect(createModuleMap(moduleDependencyGraph)).toMatchInlineSnapshot(`
        "{ '/home/kyana/code/brain-school/02-js-bundling-part-2-of-2/src/test-modules/module-c.js': function (exports,require){ const _temp = require("/home/kyana/code/brain-school/02-js-bundling-part-2-of-2/src/test-modules/folder-a/module-a.js");
        console.log(\`So yeah, \${_temp["default"]}\`); } }"
      `);
    });
  });
  describe("given a module dependency graph with a cyclical dependency", () => {
    it("returns a module map with an entry for each module", () => {
      const moduleDependencyGraph = createModuleDependencyGraph(
        __dirname,
        "./test-modules/cyclical-dependencies/module-x.js"
      );

      expect(createModuleMap(moduleDependencyGraph)).toMatchInlineSnapshot(`
        "{ '/home/kyana/code/brain-school/02-js-bundling-part-2-of-2/src/test-modules/cyclical-dependencies/module-x.js': function (exports,require){ const _temp = require("/home/kyana/code/brain-school/02-js-bundling-part-2-of-2/src/test-modules/cyclical-dependencies/module-y.js");
        const x = "hello";
        exports.default = x; } }"
      `);
    });
  });
});
