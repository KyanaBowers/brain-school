const createRuntime = (moduleMap, entryPoint) => {
  return trim(`
          const modules = ${moduleMap};
          const entry = "${entryPoint}";
          function webpackStart({modules,entry}){
            
            const moduleCache = {};
            
            //Webpack require function
            const require = moduleName => {
              //If the module is cached, return the cached version
              if(moduleCache[moduleName]){
                return moduleCache[moduleName];
              }
              const exports = {};
              //We need to avoid the cyclical dependencies
              //when invoking require()
              moduleCache[moduleName] = exports;
              
              //require() the module 
              modules[moduleName](exports,require);
              return moduleCache[moduleName];
      
            };
      
            //Execute the program
            require(entry);
            
          }
          webpackStart({modules,entry});
        `);
};

const trim = (code) => code.replace(/^\s+(.*)$/gm, "$1");

module.exports = createRuntime;
